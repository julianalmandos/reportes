<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ReclamoAdjunto
 *
 * @ORM\Table(name="reclamo_adjunto", indexes={@ORM\Index(name="IDX_B05964EE4DFFEC85", columns={"reclamo_id"})})
 * @ORM\Entity
 */
class ReclamoAdjunto
{
    /**
     * @var string
     *
     * @ORM\Column(name="adjunto", type="blob", nullable=false)
     */
    private $adjunto;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="reclamo_adjunto_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Reclamo
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Reclamo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="reclamo_id", referencedColumnName="id")
     * })
     */
    private $reclamo;



    /**
     * Set adjunto
     *
     * @param string $adjunto
     *
     * @return ReclamoAdjunto
     */
    public function setAdjunto($adjunto)
    {
        $this->adjunto = $adjunto;

        return $this;
    }

    /**
     * Get adjunto
     *
     * @return string
     */
    public function getAdjunto()
    {
        return $this->adjunto;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set reclamo
     *
     * @param \AppBundle\Entity\Reclamo $reclamo
     *
     * @return ReclamoAdjunto
     */
    public function setReclamo(\AppBundle\Entity\Reclamo $reclamo = null)
    {
        $this->reclamo = $reclamo;

        return $this;
    }

    /**
     * Get reclamo
     *
     * @return \AppBundle\Entity\Reclamo
     */
    public function getReclamo()
    {
        return $this->reclamo;
    }
}
