var map;
var RAIZ="http://reclamos.laplata.gov.ar/";

var poligonos=[];
var markers=[];

function initMap() {
  map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: -34.920495, lng: -57.953566},
      zoom: 12
  });

  map.set('styles', 
    [
      {
          "featureType": "administrative",
          "elementType": "all",
          "stylers": [
              {
                  "visibility": "simplified"
              }
          ]
      },
      {
          "featureType": "landscape",
          "elementType": "geometry",
          "stylers": [
              {
                  "visibility": "simplified"
              },
              {
                  "color": "#fcfcfc"
              }
          ]
      },
      {
          "featureType": "poi",
          "elementType": "geometry",
          "stylers": [
              {
                  "visibility": "simplified"
              },
              {
                  "color": "#fcfcfc"
              }
          ]
      },
      {
          "featureType": "road.highway",
          "elementType": "geometry",
          "stylers": [
              {
                  "visibility": "simplified"
              },
              {
                  "color": "#dddddd"
              }
          ]
      },
      {
          "featureType": "road.arterial",
          "elementType": "geometry",
          "stylers": [
              {
                  "visibility": "simplified"
              },
              {
                  "color": "#dddddd"
              }
          ]
      },
      {
          "featureType": "road.local",
          "elementType": "geometry",
          "stylers": [
              {
                  "visibility": "simplified"
              },
              {
                  "color": "#eeeeee"
              }
          ]
      },
      {
          "featureType": "water",
          "elementType": "geometry",
          "stylers": [
              {
                  "visibility": "simplified"
              },
              {
                  "color": "#dddddd"
              }
          ]
      }
    ]
  );

  //Seteo intervalo de actualizacion de información
  setInterval(function(){
    $.ajax({
      url: RAIZ+'api',
      type: "GET",
      dataType: "json",
      success: function (data)
      {
        $.each(poligonos, function(index,poligono){
          poligono.setMap(null);
        });
        $.each(markers, function(index,marker){
          marker.setMap(null);
        });
        $.each(data.jams, function(index,jam){
          var coordenadas=[];
          $.each(jam.line, function(index,punto){
            coordenadas.push({lat: punto.y, lng:punto.x});
          })
          //console.log(coordenadas);

          var poligono = new google.maps.Polygon({
            paths: coordenadas,
            strokeColor: colores[jam.level],
            strokeOpacity: 1,
            strokeWeight: 4,
            fillColor: colores[jam.level],
            fillOpacity: 1
          });
          var contenido=crearContenidoJam(jam);
          var infowindow = new google.maps.InfoWindow({
            content: contenido
          });
          poligono.addListener('click', function(e) {
            posicionActual=map.getCenter();
            zoomActual=map.getZoom();
            infowindow.setPosition(e.latLng); //No le puedo pasar un poligono al InfoWindow por lo cual le paso la posición del cursor para abrir la ventana
            infowindow.open(map);
          });
          poligono.setMap(map);
          poligonos.push(poligono);

        });

        $.each(data.alerts, function(index,alert){
          var marker = new google.maps.Marker({
            position: {lat: alert.location.y, lng: alert.location.x},
            map: map,
            title: 'Hello World!'
          });
          var contenido=crearContenidoAlerta(alert);
          var infowindow = new google.maps.InfoWindow({
            content: contenido
          });
          marker.addListener('click', function() {
            posicionActual=map.getCenter();
            zoomActual=map.getZoom();
            infowindow.open(map, marker);
            marker.setAnimation(google.maps.Animation.BOUNCE);
          });
          marker.setMap(map);
          markers.push(marker);

        });
      }
    });
  },5000);

  $('#fecha').on('change',function(){
    //console.log('entra');
    $.ajax({
      url: RAIZ+'filtrar/'+$('#fecha').val(),
      type: "GET",
      dataType: "json",
      success: function (data)
      {
        //console.log('success');
        $.each(poligonos, function(index,poligono){
          poligono.setMap(null);
        });
        $.each(markers, function(index,marker){
          marker.setMap(null);
        });
        $.each(data.jams, function(index,jam){
          var coordenadas=[];
          $.each(jam.line, function(index,punto){
            coordenadas.push({lat: punto.y, lng:punto.x});
          })
          //console.log(coordenadas);

          var poligono = new google.maps.Polygon({
            paths: coordenadas,
            strokeColor: colores[jam.level],
            strokeOpacity: 1,
            strokeWeight: 4,
            fillColor: colores[jam.level],
            fillOpacity: 1
          });
          poligono.setMap(map);
          poligonos.push(poligono);

        });
        $.each(data.alerts, function(index,alert){
          var marker = new google.maps.Marker({
            position: {lat: alert.location.y, lng: alert.location.x},
            map: map,
            title: 'Hello World!'
          });
          marker.addListener('click', function() {
            posicionActual=map.getCenter();
            zoomActual=map.getZoom();
            $('#myModal').modal('toggle');
            console.log('entra');
            marker.setAnimation(google.maps.Animation.BOUNCE);
          });
          marker.setMap(map);
          markers.push(marker);

        });
      }
    });
  });

  /*setInterval(function(){
    $.ajax({
      url: RAIZ+'guardar',
      type: "GET",
      dataType: "json",
      success: function (data)
      {
        console.log('nuevos datos guardados');
      }
    });
  },5000);*/

  //Reseteo las fechas
  $('#fecha').datepicker('setDate','0');
  fecha=$('#fecha').val();

}

//CONFIGURACION DATEPICKER
$( function() {
  $.datepicker.regional['es'] = {
    dayNamesMin: [ "Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa" ],
    monthNames: [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" ],
    monthNamesShort: [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" ],
    changeMonth: true,
    numberOfMonths: 2,
    dateFormat: 'dd-mm-yy'
  }
  $.datepicker.setDefaults($.datepicker.regional['es']);
  var dateFormat = "dd-mm-yy",
    fecha = $( "#fecha" ).datepicker({
      defaultDate: "0",          
    });

  function getDate( element ) {
    var date;
    try {
      date = $.datepicker.parseDate( dateFormat, element.value );
    } catch( error ) {
      date = null;
    }

    return date;
  }
});